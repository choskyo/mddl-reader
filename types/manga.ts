export default interface Manga {
	name: string;
	path: string;
	imagePath: string;
}

export interface Chapter {
	volume: number;
	chapter: number;
}
