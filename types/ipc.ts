export const ipc_QWE = "qwe";
export const ipc_GET_MANGA_LIST = "get-manga-list";
export const ipc_GET_MANGA_LIST_REPLY = "get-manga-list-reply";
export const ipc_GET_CHAPTER_LIST = "get-chapters-list";
export const ipc_GET_CHAPTER_LIST_REPLY = "get-chapters-list-reply";
export const ipc_GET_CHAPTER_PAGES = "get-chapter-pages";
export interface ipc_GET_CHAPTER_PAGES_ARGS {
	mangaName: string;
	volume: string;
	chapter: string;
}
export const ipc_GET_CHAPTER_PAGES_REPLY = "get-chapter-pages-reply";
