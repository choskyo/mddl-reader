export interface MddlConfig {
	manga: string[];
	archiveLocation: string;
	logPath: string;
}
