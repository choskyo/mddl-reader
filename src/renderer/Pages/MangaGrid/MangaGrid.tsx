import React, { useContext, useEffect, useState } from "react";
import Manga from "../../../../types/manga";
import { IpcContext } from "../../Contexts/IpcContext";
import MangaGridItem from "./GridItem";

const MangaGrid = () => {
	const ipcCtx = useContext(IpcContext);
	const [manga, setManga] = useState<Manga[]>([]);
	// const [filteredManga, setFilteredManga] = useState(manga);

	useEffect(() => {
		ipcCtx.ipcRenderer.send("get-manga-list");

		ipcCtx.ipcRenderer.on("get-manga-list-reply", (_, args: Manga[]) => {
			setManga(args);
		});
	}, []);

	return (
		<ul className="grid grid-cols-1 gap-6 sm:grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
			{manga.map((manga) => {
				return <MangaGridItem key={manga.name} manga={manga} />;
			})}
		</ul>
	);
};

export default MangaGrid;
