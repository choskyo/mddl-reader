import React from "react";
import { useHistory } from "react-router";
import Manga from "../../../../types/manga";

interface Props {
	manga: Manga;
}

const MangaGridItem = (props: Props) => {
	const history = useHistory();
	console.log({ manga: props.manga });

	const viewChapters = () => {
		history.push(`${props.manga.name}/chapters`);
	};

	return (
		<li className="col-span-1 flex flex-col text-center bg-white rounded-lg shadow-lg">
			<div className="flex-1 flex flex-col p-8 text-gray-900 text-sm font-medium">
				<img
					className="w-auto h-full  bg-black"
					src={`data:image/jpeg;base64,${props.manga.imagePath}`}
				/>
				<h3 className="mt-6 mb-6">{props.manga.name}</h3>
				<div className="text-gray-800 flex flex-row justify-between">
					<a onClick={() => viewChapters()}>Details</a>
					<a>Update</a>
				</div>
			</div>
		</li>
	);
};

export default MangaGridItem;
