import React from "react";
import { useHistory } from "react-router";
import { Chapter } from "../../../../types/manga";

const ChapterRow = (props: { mangaName: string; chapter: Chapter }) => {
	const history = useHistory();

	const readChapter = () => {
		console.log(
			`/read/${props.mangaName}/${props.chapter.volume}/${props.chapter.chapter}`
		);

		history.push(
			`/read/${props.mangaName}/${props.chapter.volume}/${props.chapter.chapter}`
		);
	};

	return (
		<tr>
			<td className="px-6 py-4 whitespace-nowrap">
				<div className="flex items-center">
					<div className="flex-shrink-0 h-10 w-10"></div>
					<div className="ml-4">
						<div className="text-sm font-medium text-gray-900">
							{props.chapter.chapter}
						</div>
						<div className="text-sm text-gray-500">
							{props.chapter.volume}
						</div>
					</div>
				</div>
			</td>
			<td className="px-6 py-4 whitespace-nowrap">
				<div className="text-sm text-gray-900">
					placeholder chapter name
				</div>
			</td>
			<td className="px-6 py-4 whitespace-nowrap">
				<span className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-blue-100 text-blue-800">
					Mark Read
				</span>
			</td>
			<td className="px-6 py-4 whitespace-nowrap text-sm text-gray-500">
				<span
					onClick={() => readChapter()}
					className="px-2 inline-flex text-xs leading-5 font-semibold rounded-full bg-green-100 text-green-800"
				>
					Open
				</span>
			</td>
		</tr>
	);
};

export default ChapterRow;
