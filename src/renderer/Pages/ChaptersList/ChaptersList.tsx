import React, { useContext, useEffect, useState } from "react";
import { useParams } from "react-router";
import { Chapter } from "../../../../types/manga";
import { IpcContext } from "../../Contexts/IpcContext";
import ChapterRow from "./ChapterRow";

interface RouteParams {
	mangaID: string;
}

const ChaptersList = () => {
	const routeParams = useParams<RouteParams>();
	const ipcCtx = useContext(IpcContext);
	const [chapters, setChapters] = useState<Chapter[]>([]);

	useEffect(() => {
		ipcCtx.ipcRenderer.send("get-chapters-list", routeParams.mangaID);

		console.log("sent q");

		ipcCtx.ipcRenderer.on(
			"get-chapters-list-reply",
			(_, args: Chapter[]) => {
				console.log("reply", { args });

				setChapters(args.sort((a, b) => a.chapter - b.chapter));
			}
		);
	}, []);

	return (
		<>
			<h1 className="capitalize text-4xl mb-8">{routeParams.mangaID}</h1>
			<div className="flex flex-col">
				<div className="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
					<div className="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
						<div className="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
							<table className="min-w-full divide-y divide-gray-200">
								<thead className="bg-gray-50">
									<tr>
										<th
											scope="col"
											className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
										>
											Chapter (Volume)
										</th>
										<th
											scope="col"
											className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
										>
											Name
										</th>
										<th
											scope="col"
											className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
										>
											Mark Read
										</th>
										<th
											scope="col"
											className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
										>
											Open
										</th>
									</tr>
								</thead>
								<tbody className="bg-white divide-y divide-gray-200">
									{chapters.map((c) => (
										<ChapterRow
											key={`${c.volume}/${c.chapter}`}
											mangaName={routeParams.mangaID}
											chapter={c}
										/>
									))}
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</>
	);
};

export default ChaptersList;
