import React, { useContext, useEffect, useState } from "react";
import { useParams } from "react-router";
import { ipc_GET_CHAPTER_PAGES_ARGS } from "../../../../types/ipc";
import { IpcContext } from "../../Contexts/IpcContext";

interface RouteParams {
	mangaName: string;
	volume: string;
	chapter: string;
}

const ReadChapter = () => {
	const params = useParams<RouteParams>();
	const ipcCtx = useContext(IpcContext);
	const [pages, setPages] = useState<string[]>([]);
	const [currentPage, setCurrentPage] = useState(0);

	useEffect(() => {
		const args: ipc_GET_CHAPTER_PAGES_ARGS = {
			mangaName: params.mangaName,
			volume: params.volume,
			chapter: params.chapter,
		};

		ipcCtx.ipcRenderer.send("get-chapter-pages", args);

		ipcCtx.ipcRenderer.on("get-chapter-pages-reply", (_, args) => {
			setPages(args);
		});
	}, []);

	return (
		<>
			<h1 className="capitalize text-4xl mb-8">
				{params.mangaName}, Volume {params.volume} Chapter{" "}
				{params.chapter}
			</h1>
			<img src={`data:image/jpeg;base64,${pages[currentPage]}`} />
			<div className="flex justify-around w-full py-8">
				<button onClick={() => setCurrentPage(currentPage - 1)}>
					prev
				</button>
				<button onClick={() => setCurrentPage(currentPage + 1)}>
					next
				</button>
			</div>
		</>
	);
};

export default ReadChapter;
