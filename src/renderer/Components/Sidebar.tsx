import React from "react";
import SidebarLink from "./SidebarLink";

const Sidebar = () => {
	return (
		<div className="hidden md:flex md:flex-shrink-0">
			<div className="w-64 flex flex-col">
				<div className="border-r border-gray-200 pt-5 pb-4 flex flex-col flex-grow overflow-y-auto">
					<div className="flex-shrink-0 px-4 flex items-center">
						<h1 className="h-8 w-auto text-center text-xl text-gray-800">
							MDDL Reader
						</h1>
					</div>
					<div className="flex-grow mt-5 flex flex-col">
						<nav className="flex-1 bg-white px-2 space-y-1">
							<SidebarLink path="/" title="Manga" />
							<SidebarLink
								path="/settings"
								title="Innstillinger"
							/>
						</nav>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Sidebar;
