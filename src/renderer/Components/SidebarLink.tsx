import React, { useMemo } from "react";
import { Link, useLocation } from "react-router-dom";

interface Props {
	icon?: React.ReactElement;
	title: string;
	path: string;
}

const SidebarLink = (props: Props) => {
	const loc = useLocation();

	const active = useMemo(() => {
		return loc.pathname === props.path;
	}, [loc.pathname]);

	const colours = active
		? "text-white hover:bg-blue-500 bg-blue-800"
		: "text-gray-600 hover:bg-gray-50 hover:text-gray-900";

	return (
		<Link
			to={props.path}
			className={`${colours} group rounded-md py-2 px-2 flex items-center text-sm font-medium`}
		>
			{props.icon}
			{props.title}
		</Link>
	);
};

export default SidebarLink;
