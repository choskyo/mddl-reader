const { ipcRenderer } = require("electron");
import React from "react";
import ReactDOM from "react-dom";
import { HashRouter } from "react-router-dom";
import App from "./App";
import { IpcContext } from "./Contexts/IpcContext";
import "./index.css";

ReactDOM.render(
	<React.StrictMode>
		<IpcContext.Provider value={{ ipcRenderer }}>
			<HashRouter>
				<App />
			</HashRouter>
		</IpcContext.Provider>
	</React.StrictMode>,
	document.getElementById("root") as HTMLDivElement
);

// Hot Module Replacement (HMR) - Remove this snippet to remove HMR.
// Learn more: https://www.snowpack.dev/concepts/hot-module-replacement
// if (import.meta.hot) {
// import.meta.hot.accept();
// }
