import React from "react";

interface CtxType {
	ipcRenderer: Electron.IpcRenderer;
}

export const IpcContext = React.createContext<CtxType>(null);
