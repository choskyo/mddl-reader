import React from "react";
import { Route, Switch } from "react-router";
import Sidebar from "./Components/Sidebar";
import ChaptersList from "./Pages/ChaptersList/ChaptersList";
import MangaGrid from "./Pages/MangaGrid/MangaGrid";
import ReadChapter from "./Pages/ReadChapter/ReadChapter";

const App = () => {
	return (
		<div className="h-screen bg-white overflow-hidden flex">
			<Sidebar />
			<div className="bg-gray-100 flex-1 w-0 flex flex-col">
				<main className="flex-1 relative overflow-y-auto focus:outline-none">
					<div className="p-8">
						<Switch>
							<Route exact path="/">
								<MangaGrid />
							</Route>
							<Route exact path="/:mangaID/chapters/">
								<ChaptersList />
							</Route>
							<Route
								exact
								path="/read/:mangaName/:volume/:chapter"
							>
								<ReadChapter />
							</Route>
						</Switch>
					</div>
				</main>
			</div>
		</div>
	);
};

export default App;
