import { app, ipcMain } from "electron";
import fs from "fs/promises";
import path from "path";
import {
	ipc_GET_CHAPTER_LIST,
	ipc_GET_CHAPTER_LIST_REPLY,
	ipc_GET_CHAPTER_PAGES,
	ipc_GET_CHAPTER_PAGES_ARGS,
	ipc_GET_CHAPTER_PAGES_REPLY,
	ipc_GET_MANGA_LIST,
} from "../../types/ipc";
import { Chapter } from "../../types/manga";
import { MddlConfig } from "../../types/mddl";

const mddlConfigPath = path.join(app.getPath("appData"), "mddl", "config.json");
let mddlConfig: MddlConfig;

fs.readFile(mddlConfigPath).then((buf) => {
	mddlConfig = JSON.parse(buf.toString()) as MddlConfig;
});

ipcMain.on(ipc_GET_MANGA_LIST, async (event, _) => {
	const mangaDirContents = await fs.readdir(mddlConfig.archiveLocation);

	const mangaList = await Promise.all(
		mangaDirContents.map(async (m) => {
			let imagePath = "";

			try {
				const firstVolume = (
					await fs.readdir(path.join(mddlConfig.archiveLocation, m))
				)[0];

				const firstChapter = (
					await fs.readdir(
						path.join(mddlConfig.archiveLocation, m, firstVolume)
					)
				)[0];

				const firstImage = (
					await fs.readdir(
						path.join(
							mddlConfig.archiveLocation,
							m,
							firstVolume,
							firstChapter
						)
					)
				)[0];

				imagePath = (
					await fs.readFile(
						path.join(
							mddlConfig.archiveLocation,
							m,
							firstVolume,
							firstChapter,
							firstImage
						)
					)
				).toString("base64");
			} catch (err) {
				console.log(err);
			}

			return {
				name: m,
				path: path.join(mddlConfig.archiveLocation, m),
				imagePath,
			};
		})
	);

	event.reply("get-manga-list-reply", mangaList);
});

ipcMain.on(ipc_GET_CHAPTER_LIST, async (event, mangaName: string) => {
	const mangaPath = path.join(mddlConfig.archiveLocation, mangaName);

	const volumesList = await fs.readdir(mangaPath);

	const chapters = (
		await Promise.all(
			volumesList.map(async (vol) => {
				const volChapters = await fs.readdir(path.join(mangaPath, vol));

				return volChapters.map(
					(vc): Chapter => {
						return {
							chapter: +vc,
							volume: +vol,
						};
					}
				);
			})
		)
	).flat(1);

	event.reply(ipc_GET_CHAPTER_LIST_REPLY, chapters);
});

ipcMain.on(
	ipc_GET_CHAPTER_PAGES,
	async (event, args: ipc_GET_CHAPTER_PAGES_ARGS) => {
		console.log("args", args);

		const chapterPath = path.join(
			mddlConfig.archiveLocation,
			args.mangaName,
			args.volume,
			args.chapter
		);

		const pagesList = await fs.readdir(chapterPath);

		const pages = (
			await Promise.all(
				pagesList.map(async (pageFileName) => {
					return (
						await fs.readFile(path.join(chapterPath, pageFileName))
					).toString("base64");
				})
			)
		).flat(1);

		event.reply(ipc_GET_CHAPTER_PAGES_REPLY, pages);
	}
);
